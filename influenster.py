
# coding: utf-8

# In[2]:


import json
import sys
import numpy as np
#importing the json data
with open("C:/Users/osman/Desktop/coding/Drunk Elephant C-Firma Day Serum.json") as file:
    data = json.load(file)


# In[2]:


#aha! the json data is a dictionary split between the 3 review sources.
print(data['Drunk Elephant C-Firma Day Serum'].keys())


##NOTES:
# So far, I've put some generic words (good, love, etc.) into the word banks for each category. But that's a cheap shortcut.
# Someone could be using good to indicate texture, efficacy, or just general goodness.
# A more rigorous approach:
#    run sentiment analysis per sentence
#    have word banks for each category (positive, negative, or neutral) to associate each sentence to a category
#    one sentence category ID'd, use final word bank of generic words (love, good, horrible) to further qualify degree of sentiment


# In[3]:


iReviews = data['Drunk Elephant C-Firma Day Serum']['influenster'] #focusing solely on influenster reviews for now.
print(iReviews[0].keys())
print(iReviews[1])

#some negative influenster reviews: 156, 1580, 7


# In[42]:


#number of times a particular term is found in review body
def countTerm(reviews,term):
    count = 0
    for item in reviews:
        if term in item['text']:
            count = count + 1
    return count

#returns indices of reviews possessing this term
def reviewIndex(reviews,term):
    hits = []
    count = 0
    for review in reviews:
        if term in review['text']:
            hits.append(count)
        count = count + 1
    return hits

#prints all reviews with indices listed in "hits"
def printReviews(reviews,hits):
    for count in hits:
        print(reviews[count]['text'])
    return
        
import re
#taken from https://stackoverflow.com/questions/4576077/python-split-text-on-sentences
caps = "([A-Z])"
prefixes = "(Mr|St|Mrs|Ms|Dr)[.]"
suffixes = "(Inc|Ltd|Jr|Sr|Co)"
starters = "(Mr|Mrs|Ms|Dr|He\s|She\s|It\s|They\s|Their\s|Our\s|We\s|But\s|However\s|That\s|This\s|Wherever)"
acronyms = "([A-Z][.][A-Z][.](?:[A-Z][.])?)"
websites = "[.](com|net|org|io|gov)"

#splits text into separate sentences - will be important later!
def split_into_sentences(text):
    text = " " + text + "  "
    text = text.replace("\n"," ")
    text = re.sub(prefixes,"\\1<prd>",text)
    text = re.sub(websites,"<prd>\\1",text)
    if "Ph.D" in text: text = text.replace("Ph.D.","Ph<prd>D<prd>")
    text = re.sub("\s" + caps + "[.] "," \\1<prd> ",text)
    text = re.sub(acronyms+" "+starters,"\\1<stop> \\2",text)
    text = re.sub(caps + "[.]" + caps + "[.]" + caps + "[.]","\\1<prd>\\2<prd>\\3<prd>",text)
    text = re.sub(caps + "[.]" + caps + "[.]","\\1<prd>\\2<prd>",text)
    text = re.sub(" "+suffixes+"[.] "+starters," \\1<stop> \\2",text)
    text = re.sub(" "+suffixes+"[.]"," \\1<prd>",text)
    text = re.sub(" " + caps + "[.]"," \\1<prd>",text)
    if "”" in text: text = text.replace(".”","”.")
    if "\"" in text: text = text.replace(".\"","\".")
    if "!" in text: text = text.replace("!\"","\"!")
    if "?" in text: text = text.replace("?\"","\"?")
    text = text.replace(".",".<stop>")
    text = text.replace("?","?<stop>")
    text = text.replace("!","!<stop>")
    text = text.replace("<prd>",".")
    sentences = text.split("<stop>")
    sentences = sentences[:-1]
    sentences = [s.strip() for s in sentences]
    return sentences

#quick and dirty test to ID reviews that dislike texture vs like texture
def textureTest(text):
    goodWords = ['smooth','radiant','moisturized','consistency','consistent','amazing','absorbed','seeps','fast acting','light','comfortable','good','healthy','love','loving','bright','wrinkle','wrinkly']
    badWords  = ['oily','greasy','shiny','residue','heavy','sticky','stickiness','tacky','odd','sticky forever','itchy','hate','annoying','liquefy','clog','drying','burn','peeling','gritty','irritate','suck','stings','stiff','film']
    goodCount = 0
    badCount  = 0
    
    for word in goodWords:
        goodCount = goodCount + text.count(word) - text.count("not " + word) - text.count("n't "+word) - 2*text.count("never "+ word)
    for word in badWords:
        badCount  = badCount  + text.count(word) - text.count("not " + word) - text.count("n't "+word) - 2*text.count("never "+ word)
        
    return(goodCount-badCount)


#quick and dirty test to ID reviews that say this product is effective vs ineffective
def efficacyTest(text):
    goodWords = ['worth it','visible','results','nicer','really liked','really works','visibly','enriching','love it','powerful','value','favorite','amazing','well worth','worth','repurchase','helped reduce','compliment','fast acting','would recommend','hooked','good','effective','useful','change','quality','amazing','incredible','love','help','excellent','healthy','potent','love','loving','improve','like']
    badWords  = ['horrible','difficult','unfortunately','doubt','not sure','let down','garbage','hate','disappoint','ineffective','useless','defective','expired','broken','gimmick','break out','upset','nightmare','wrong','do not use','broke out','returned','regret','shame']
    goodCount = 0
    badCount  = 0
    
    for word in goodWords:
        goodCount = goodCount + text.count(word) - 2*text.count("not " + word) - 2*text.count("n't "+word) - 2*text.count("never "+ word)
    for word in badWords:
        badCount  = badCount  + text.count(word) - 2*text.count("not " + word) - 2*text.count("n't "+word) - 2*text.count("never "+ word)
    
    return(goodCount-badCount)

#quick and dirty test to ID reviews that say this product plays nice w/ makeup vs note
def makeupTest(text):
    goodWords = ['played well','mix','ease','healthy','apply','last','great','good','love','like','fine']
    neutWords = ['react','layer','apply','makeup','cosmetic','underneath makeup','primer','foundation','routine','under','over','before','wear']
    badWords  = ['difficult','shiny',"can't wear",'not putting makeup','liquefy','ruined','nightmare','balling','affect','bleed','blot','slip','pills','hate']
    goodCount = 0
    badCount  = 0
    neutCount = 0
    
    for word in goodWords:
        goodCount = goodCount + text.count(word) - 2*text.count("not " + word) - 2*text.count("n't "+word) - 2*text.count("never "+ word)
    for word in badWords:
        badCount  = badCount  + text.count(word) - 2*text.count("not " + word) - 2*text.count("n't "+word) - 2*text.count("never "+ word)
    for word in neutWords:
        neutCount = neutCount + text.count(word)
    
    if neutCount<2:
        neutCount=0
    if ((goodCount-badCount)*(goodCount-badCount))<5:
        neutCount=0
    
    return((goodCount-badCount)*neutCount)


# In[45]:


print(countTerm(iReviews,"n't" + " feel"))
hits = reviewIndex(iReviews,"sticky")


# In[ ]:


for review in hits:
    print(iReviews[review]['text'])
    print(iReviews[review]['stars']+" stars")
    print(textureTest(iReviews[review]['text']))


# In[53]:


scoreListTexture = []
for review in iReviews[:2600]:
    score = textureTest(review['text'])
    scoreListTexture.append(score)
    if review['stars']=='5' and score<0:
        score = score/2
    elif int(review['stars'])<5 and score<0:
        score = score * (6-int(review['stars']))
    elif review['stars']=='5' and score>0:
        score = score+1
    if score >3:
        print(review['text'])
        print(review['stars']+ " stars")
        print("good texture! score of "+ str(score))
    if score <-3:
        print(review['text'])
        print(review['stars']+ " stars")
        print("bad texture! score of "+ str(score))


# In[60]:


import matplotlib.pyplot as plt

#np.histogram(scoreList)

plt.hist(scoreListTexture, bins='auto')  # arguments are passed to np.histogram
plt.title("Histogram with 'auto' bins for texture Scores")
plt.show()

plt.hist(scoreListEfficacy, bins='auto')  # arguments are passed to np.histogram
plt.title("Histogram with 'auto' bins for efficacy Scores")
plt.show()

plt.hist(scoreListMakeup, bins=100)  # arguments are passed to np.histogram
plt.title("Histogram with 'auto' bins for makeup Scores")
plt.ylim(0,100)
plt.show()


# In[52]:


scoreListEfficacy = []
for review in iReviews[:2600]:
    score = efficacyTest(review['text'])
    scoreListEfficacy.append(score)
    if review['stars']=='5' and score<0:
        score = score/2
    elif int(review['stars'])<5 and score<0:
        score = score * (6-int(review['stars']))
    if score >3:
        print(review['text'])
        print(review['stars']+ " stars")
        print("really effective! score of "+ str(score))
    if score <-3:
        print(review['text'])
        print(review['stars']+ " stars")
        print("really not effective! score of "+ str(score))


# In[51]:


scoreListMakeup = []
for review in iReviews[:2600]:
    score = makeupTest(review['text'])
    scoreListMakeup.append(score)
    if review['stars']=='5' and score<0:
        score = score/2
#    elif int(review['stars'])<5 and score<0:
#        score = score * (6-int(review['stars']))
    if score >5:
        print(review['text'])
        print(review['stars']+ " stars")
        print("fine with Makeup! score of "+ str(score))
    if score <-5:
        print(review['text'])
        print(review['stars']+ " stars")
        print("not good with Makeup! score of "+ str(score))

